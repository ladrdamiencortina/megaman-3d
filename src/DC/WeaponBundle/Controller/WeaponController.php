<?php

namespace DC\WeaponBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use DC\WeaponBundle\Entity\Weapon;

use Symfony\Component\HttpFoundation\JsonResponse;

class WeaponController extends Controller
{
    /**
     * @Route("/armory/{id}", name="armory", options={"expose":true})
     */
    public function armoryAction(Request $request, Weapon $weapon)
    {
        if($request->isXmlHttpRequest()) {
          $weaponId = $request->get('weapon_id');
          $em = $this->getDoctrine()->getManager();
          return new JsonResponse(array(
              'name' => $weapon->getName(),
              'dammage' => $weapon->getDammage(),
              'range' => $weapon->getRange(),
              'fireRate' => $weapon->getFireRate(),
              'image' => $weapon->getImage()
          ),200);
        }else{
          throw $this->createNotFoundException("La page n'existe pas");
        }
    }
}
