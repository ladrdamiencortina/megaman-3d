<?php
// src/DC/UserBundle/Entity/User.php

namespace DC\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

use DC\CharacterBundle\Entity\Character;

/**
 * @ORM\Table(name="dc_user")
 * @ORM\Entity(repositoryClass="DC\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
  /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @var integer
   *
   * @ORM\ManyToOne(targetEntity="DC\CharacterBundle\Entity\Character")
   * @ORM\JoinColumn(name="character_id", referencedColumnName="id")
   */
  private $character;

  /**
   * Set character.
   *
   * @param Character $character
   */
  public function setCharacter($character)
  {
      $this->character = $character;

      return $this;
  }

  /**
   * Get character.
   *
   * @return Character
   */
  public function getCharacter()
  {
      return $this->character;
  }
}
