<?php

namespace DC\TextureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DC\TextureBundle\Entity\Texture;

class CreateTextureCommand extends ContainerAwareCommand
{

  /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:create_texture')
            ->setDescription('Commande de creation de texture')
            ->addArgument('name')
            ->addArgument('wall')
            ->addArgument('floor')
            ->addArgument('ceiling')
            ->addArgument('width')
            ->addArgument('height')
            ->addArgument('depth');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
      $textureCreator = $this->getContainer()->get('dc_texture.texturecreator');
      $em = $this->getContainer()->get('doctrine')->getManager();
      $wallTexture = $em->getRepository(Texture::class)->find($input->getArgument('wall'));
      $floorTexture = $em->getRepository(Texture::class)->find($input->getArgument('floor'));
      $ceilingTexture = $em->getRepository(Texture::class)->find($input->getArgument('ceiling'));
      $textureCreator->createMeshTexture($input->getArgument('name'), $floorTexture, $wallTexture,  $ceilingTexture,$input->getArgument('width'),$input->getArgument('height'),$input->getArgument('depth'));
      $output->writeln("<info>Texture ".$input->getArgument('name')." crée ! </info>");

    }

}
