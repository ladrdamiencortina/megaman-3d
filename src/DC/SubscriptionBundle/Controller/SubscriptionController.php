<?php

namespace DC\SubscriptionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/inscription")
 */
class SubscriptionController extends Controller
{

  /**
   * First page of the subscription process, that presents the game and the process
   *
   * @Route("/")
   */
  public function indexAction()
  {
      return $this->render('DCSubscriptionBundle:Subscription:step1.html.twig');
  }

    /**
     * Personnal informations (first name, last name, email)
     *
     * @Route("/etape1")
     */
    public function step1Action()
    {
        return $this->render('DCSubscriptionBundle:Subscription:step1.html.twig');
    }

    /**
     * Newsletter consent (or not)
     *
     * @Route("/etape2")
     */
    public function step2Action()
    {
        return $this->render('DCSubscriptionBundle:Subscription:step2.html.twig');
    }

    /**
     * Newsletter consent (or not)
     *
     * @Route("/etape3")
     */
    public function step3Action()
    {
        return $this->render('DCSubscriptionBundle:Subscription:step3.html.twig');
    }

    /**
     * Character page (pseudo and main class choice)
     *
     * @Route("/etape4")
     */
    public function step4Action()
    {
        return $this->render('DCSubscriptionBundle:Subscription:step4.html.twig');
    }

    /**
     * Indicate that process is ended, welcome in game 0_0
     *
     * @Route("/termine")
     */
    public function finalAction()
    {
        return $this->render('DCSubscriptionBundle:Subscription:final.html.twig');
    }
}
