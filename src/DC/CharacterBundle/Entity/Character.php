<?php

namespace DC\CharacterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use DC\WeaponBundle\Entity\Weapon;

/**
 * Class
 *
 * @ORM\Table(name="dc_character")
 * @ORM\Entity(repositoryClass="DC\CharacterBundle\Repository\ClassRepository")
 */
class Character
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="character_sprite_still", type="string", length=255)
     */
    private $spriteStill;

    /**
     * @var string
     *
     * @ORM\Column(name="character_sprite_walk", type="string", length=255)
     */
    private $spriteWalk;

    /**
     * @var string
     *
     * @ORM\Column(name="character_sprite_attack", type="string", length=255)
     */
    private $spriteAttack;

    /**
     * @var string
     *
     * @ORM\Column(name="character_sprite_hurt", type="string", length=255)
     */
    private $spriteHurt;

    /**
     * @var string
     *
     * @ORM\Column(name="character_sprite_dead", type="string", length=255)
     */
    private $spriteDead;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var int
     *
     * @ORM\Column(name="speed", type="decimal", precision=2, scale=1)
     */
    private $speed;

    /**
     * @var Weapon
     *
     * @ORM\ManyToOne(targetEntity="DC\WeaponBundle\Entity\Weapon")
     * @ORM\JoinColumn(name="default_weapon_id", referencedColumnName="id")
     */
    private $defaultWeapon;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Class
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set spriteStill.
     *
     * @param string $spriteStill
     *
     * @return Character
     */
    public function setSpriteStill($spriteStill)
    {
        $this->spriteStill = $spriteStill;

        return $this;
    }

    /**
     * Get spriteStill.
     *
     * @return string
     */
    public function getSpriteStill()
    {
        return $this->spriteStill;
    }

    /**
     * Set spriteWalk.
     *
     * @param string $spriteWalk
     *
     * @return Character
     */
    public function setSpriteWalk($spriteWalk)
    {
        $this->spriteWalk = $spriteWalk;

        return $this;
    }

    /**
     * Get spriteWalk.
     *
     * @return string
     */
    public function getSpriteWalk()
    {
        return $this->spriteWalk;
    }

    /**
     * Set spriteAttack.
     *
     * @param string $spriteAttack
     *
     * @return Character
     */
    public function setSpriteAttack($spriteAttack)
    {
        $this->spriteAttack = $spriteAttack;

        return $this;
    }

    /**
     * Get spriteAttack.
     *
     * @return string
     */
    public function getSpriteAttack()
    {
        return $this->spriteAttack;
    }

    /**
     * Set spriteDead.
     *
     * @param string $spriteDead
     *
     * @return Character
     */
    public function setSpriteDead($spriteDead)
    {
        $this->spriteDead = $spriteDead;

        return $this;
    }

    /**
     * Get spriteDead.
     *
     * @return string
     */
    public function getSpriteDead()
    {
        return $this->spriteDead;
    }

    /**
     * Set spriteHurt.
     *
     * @param string $spriteHurt
     *
     * @return Character
     */
    public function setSpriteHurt($spriteHurt)
    {
        $this->spriteHurt = $spriteHurt;

        return $this;
    }

    /**
     * Get spriteHurt.
     *
     * @return string
     */
    public function getSpriteHurt()
    {
        return $this->spriteHurt;
    }

    /**
     * Set Speed.
     *
     * @param int $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed.
     *
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set defaultWeapon.
     *
     * @param \DC\WeaponBundle\Entity\Weapon|null $defaultWeapon
     *
     * @return Character
     */
    public function setDefaultWeapon(\DC\WeaponBundle\Entity\Weapon $defaultWeapon = null)
    {
        $this->defaultWeapon = $defaultWeapon;

        return $this;
    }

    /**
     * Get defaultWeapon.
     *
     * @return \DC\WeaponBundle\Entity\Weapon|null
     */
    public function getDefaultWeapon()
    {
        return $this->defaultWeapon;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Character
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Character
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}
