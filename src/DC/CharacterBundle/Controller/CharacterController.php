<?php

namespace DC\CharacterBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use DC\CharacterBundle\Entity\Character;

use Symfony\Component\HttpFoundation\JsonResponse;

class CharacterController extends Controller
{

      /**
       * @Route("/personnages", name="character_select")
       */
      public function characterAction(Request $request)
      {
          $em = $this->getDoctrine()->getManager();
          $characters = $em->getRepository(Character::class)->findAll();
          return $this->render('default/characterSelect.html.twig', array("characters"=>$characters));
      }

    /**
     * @Route("/loadPlayer", name="loadPlayer", options={"expose":true})
     */
    public function loadPlayerAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
          $em = $this->getDoctrine()->getManager();
          // $character = $em->getRepository(Character::class)->find($this->getUser()->getCharacter());
          return new JsonResponse(array(
              'speed' => $this->getUser()->getCharacter()->getSpeed(),
              'spriteStill' => $this->getUser()->getCharacter()->getSpriteStill(),
              'spriteWalk' => unserialize($this->getUser()->getCharacter()->getSpriteWalk()),
              'spriteAttack' => unserialize($this->getUser()->getCharacter()->getSpriteAttack()),
              'spriteHurt' => unserialize($this->getUser()->getCharacter()->getSpriteHurt()),
              'spriteDead' => unserialize($this->getUser()->getCharacter()->getSpriteDead()),
              'defaultWeapon' => $this->getUser()->getCharacter()->getDefaultWeapon()->getId()
          ),200);
        }else{
          throw $this->createNotFoundException("La page n'existe pas");
        }
    }

    /**
     * @Route("/characterSelect", name="characterSelect", options={"expose":true})
     */
    public function characterSelectAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
          $characterId = $request->get('character');
          $em = $this->getDoctrine()->getManager();
          $character = $em->getRepository(Character::class)->find($characterId);
          $this->getUser()->setCharacter($character);
          $em->persist($this->getUser());
          $em->flush();
          return new JsonResponse(array(),200);
        }else{
          throw $this->createNotFoundException("La page n'existe pas");
        }
    }
}
