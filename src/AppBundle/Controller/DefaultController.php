<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use DC\CharacterBundle\Entity\Character;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/homepage.html.twig', []);
    }

    /**
     * @Route("/play", name="game")
     */
    public function gameAction(Request $request)
    {
        if($this->getUser()->getCharacter() == null){
          $this->addFlash('warning', 'Choisissez un personnage avant de commencer à jouer !');
          return $this->redirectToRoute('character_select');
        }
        return $this->render('default/game.html.twig', []);
    }
}
