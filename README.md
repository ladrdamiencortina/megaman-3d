# OpenClassrooms P5

This project is an online multiplayer game based on socket.io where each player has to fight other players. 3d is controlled with the webgl API through babylon.js. The rest of the site is build with symfony 3.4.

Site available at damien-cortina.com
User : name : openclassrooms, password : azerty

## Installation

You first need to clone the site and the socket

To install the site :

`composer install`

`php bin/console doctrine:schema:update --force`

To install the socket :

`npm install`

To launch the socket : 

`node index.js`

A database export is available (database.sql)
User : name : openclassrooms, password : azerty

## Graphic assets

 All ingame graphic assets (characters, weapons, wall textures) are extracted from Delver, a game by to Priority Interupt