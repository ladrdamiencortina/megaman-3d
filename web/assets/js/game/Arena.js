Arena = function(game) {

    this.game = game;
    var scene = game.scene;


    //--------------------------------------------------------------------------
    // Lights
    //--------------------------------------------------------------------------

    // Light for the ceiling
    var bottomLight = new BABYLON.HemisphericLight("bottomLight", new BABYLON.Vector3(0, -100, 0), scene);
    bottomLight.intensity = 0.5;
    bottomLight.specular = new BABYLON.Color3(0,0,0);

    // Light for the floor
    var topLight = new BABYLON.HemisphericLight("topLight", new BABYLON.Vector3(0, 100, 0), scene);
    topLight.intensity = 0.5;
    topLight.specular = new BABYLON.Color3(0,0,0);

    // Colored light simulating the torch
    var torchLight1 = new BABYLON.PointLight("pointLight", new BABYLON.Vector3(0, 0, 6*32-4), scene);
    torchLight1.specular = new BABYLON.Color3(0, 0, 0);
	  torchLight1.diffuse = new BABYLON.Color3(253, 106, 2);
    torchLight1.intensity = 0.001;

    var torchLight2 = new BABYLON.PointLight("pointLight", new BABYLON.Vector3(0, 0, -6*32+4), scene);
    torchLight2.specular = new BABYLON.Color3(0, 0, 0);
	  torchLight2.diffuse = new BABYLON.Color3(253, 106, 2);
    torchLight2.intensity = 0.001;

    var shadowGenerator1 = new BABYLON.ShadowGenerator(2048, torchLight1);
    shadowGenerator1.usePoissonSampling = true;
    shadowGenerator1.bias = 0.0005;

    var shadowGenerator2 = new BABYLON.ShadowGenerator(2048, torchLight2);
    shadowGenerator2.usePoissonSampling = true;
    shadowGenerator2.bias = 0.0005;


    //--------------------------------------------------------------------------
    // Materials
    //--------------------------------------------------------------------------

    var roomTexture = new BABYLON.StandardMaterial("quickRoom", scene);
    roomTexture.diffuseTexture = new BABYLON.Texture("../../assets/textures/created/big_room.png", scene);

    var columnTexture = new BABYLON.StandardMaterial("quickColumn",scene);
    columnTexture.diffuseTexture =  new BABYLON.Texture("../../assets/textures/created/collone_test.png", scene);

    var colExtremitiesTexture = new BABYLON.StandardMaterial("colExtremities", scene);
    colExtremitiesTexture.diffuseTexture = new BABYLON.Texture("../../assets/textures/created/base_collone_test.png", scene);


    //--------------------------------------------------------------------------
    // Sprites
    //--------------------------------------------------------------------------

    var spriteManagerTorch = new BABYLON.SpriteManager("torchManager", "../../assets/textures/origin/sprites.png", 2, 32, scene);

    var torch1 = new BABYLON.Sprite("torch1", spriteManagerTorch);
    torch1.playAnimation(32, 38, true, 100);
    torch1.size = 25;
    torch1.position = new BABYLON.Vector3(0, 0, 6*32-4);

    var torch2 = new BABYLON.Sprite("torch2", spriteManagerTorch);
    torch2.playAnimation(32, 38, true, 100);
    torch2.size = 25;
    torch2.position = new BABYLON.Vector3(0, 0, -6*32+4);


    //--------------------------------------------------------------------------
    // Meshs
    //--------------------------------------------------------------------------

    //Walls, floor, ceiling...
    var room = BABYLON.MeshBuilder.CreateBox("room",getOptions(32*12,32*2,32*12,BABYLON.Mesh.BACKSIDE),scene);
    room.material = roomTexture;
    room.checkCollisions = true;
    room.receiveShadows = true;

    //Columns
    var columns = [];
    var numberColumn = 12;
    console.log(room.scaling.x);
    var sizeArena = 32*24*room.scaling.x;
    var borderWidth = ((100/numberColumn)/100)*sizeArena/2;
    var ratio = borderWidth*1.5;
    for (var i = 0; i <= 1; i++) {
      if(numberColumn>0){
          columns[i] = [];

          // Column
          let mainColCube = BABYLON.MeshBuilder.CreateBox("col0-"+i, getOptions(16,78,16,BABYLON.Mesh.FRONTSIDE), scene);
          mainColCube.material = columnTexture;
          mainColCube.position = new BABYLON.Vector3(-sizeArena/2+ borderWidth ,0,-40 + (80 * i));
          mainColCube.checkCollisions = true;
          mainColCube.maxSimultaneousLights = 10;
          shadowGenerator1.getShadowMap().renderList.push(mainColCube);
          shadowGenerator2.getShadowMap().renderList.push(mainColCube);
          mainColCube.receiveShadows = true;

          // Bottom of the column
          var colBottom = BABYLON.MeshBuilder.CreateBox("colBottom",getOptions(20,10,20,BABYLON.Mesh.FRONTSIDE),scene);
          colBottom.parent = mainColCube;
          colBottom.material = colExtremitiesTexture;
          colBottom.position = new BABYLON.Vector3(0 ,-27 ,0);
          colBottom.maxSimultaneousLights = 10;
          shadowGenerator1.getShadowMap().renderList.push(colBottom);
          shadowGenerator2.getShadowMap().renderList.push(colBottom);
          colBottom.receiveShadows = true;

          // Top of the column
          var colTop = colBottom.clone("colTop");
          colTop.position = new BABYLON.Vector3(0 ,27 ,0);
          colTop.maxSimultaneousLights = 10;
          shadowGenerator1.getShadowMap().renderList.push(colTop);
          shadowGenerator2.getShadowMap().renderList.push(colTop);
          colTop.receiveShadows = true;

          columns[i].push(mainColCube);
          if(numberColumn>1){
              for (let y = 1; y <= numberColumn - 1; y++) {
                  let newColCube = columns[i][0].clone("col"+y+"-"+i);
                  newColCube.position = new BABYLON.Vector3(-(sizeArena/2)+ borderWidth + (borderWidth*2*y) ,0,columns[i][0].position.z);
                  newColCube.checkCollisions = true;
                  newColCube.maxSimultaneousLights = 10;
                  shadowGenerator1.getShadowMap().renderList.push(newColCube);
                  shadowGenerator2.getShadowMap().renderList.push(newColCube);
                  newColCube.receiveShadows = true;
                  columns[i].push(newColCube);
              }
          }
      }
    }

    //--------------------------------------------------------------------------
    // Functions
    //--------------------------------------------------------------------------

    // Obtention des options pour les meshs utilisants MeshBuilder
    function getOptions(width, height, depth, side)
    {
      var mapU = 4*width + 2*height;
      if(height<depth)
      {
        var mapV = depth;
        var h = height;
      }else{
        var mapV = height;
        var h = height;
      }
      var faceUV = new Array(6);
        faceUV[0] = new BABYLON.Vector4(width/mapU, 1-h/mapV , 2*width/mapU,1); // Face arrière
        faceUV[1] = new BABYLON.Vector4(0, 1-h/mapV, width/mapU,1 );  // Face avant
        faceUV[2] = new BABYLON.Vector4(((2*width)+height)/mapU,1-depth/mapV ,(2*width)/mapU, 1);  // Face latérale
        faceUV[3] = new BABYLON.Vector4((2*width+2*height)/mapU, 1-depth/mapV , (2*width+height)/mapU, 1);  // Face latérale
        faceUV[4] = new BABYLON.Vector4(((2*width)+2*height)/mapU, 1, (3*width+2*height)/mapU, 1-depth/mapV);  // Face supérieure
        faceUV[5] = new BABYLON.Vector4((3*width+2*height)/mapU, 1, (4*width+2*height)/mapU, 1-depth/mapV);  // Face inférieure
      var options = {
        width:width,  // Largeur du mesh
        height:height,  // Hauteur du mesh
        depth:depth,  // Profondeur du mesh
        faceUV: faceUV,  // Portion d'image utilisée par chaque face du mesh
        sideOrientation: side // Wich side of the mesh is rendered (inner, outter, both...)
      };
      return options;
    }

    var convertToFlat = function () {
        for (var index = 0; index < scene.textures.length; index++) {
            scene.textures[index].updateSamplingMode(BABYLON.Texture.NEAREST_SAMPLINGMODE);
        }
    }

    scene.executeWhenReady(function() {
        convertToFlat();
    });

};
