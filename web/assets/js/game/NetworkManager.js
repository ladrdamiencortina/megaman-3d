// ================================================
// DECLARATION SOCKET
var socket;
var promise1 = new Promise(function(resolve, reject) {
var httpRequest = new XMLHttpRequest();
if (!httpRequest) {
    console.log('Impossible de créer la requète');
}
var that = this;
var myData = null;

httpRequest.onreadystatechange = function(){
  try{
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        var characterData = JSON.parse(httpRequest.responseText);
        myData = characterData;
        socket = io.connect('http://localhost:8080', { query: "characterSpeed="+characterData.speed+"&characterSpriteStill="+characterData.spriteStill+"&characterSpriteWalk="+JSON.stringify(characterData.spriteWalk)+"&characterSpriteAttack="+JSON.stringify(characterData.spriteAttack)+"&characterSpriteHurt="+JSON.stringify(characterData.spriteHurt)+"&characterSpriteDead="+JSON.stringify(characterData.spriteDead)+"&characterDefaultWeapon="+characterData.defaultWeapon });
        resolve();
      } else {
        alert('Erreur dans la requète.');
      }
    }
  }
  catch( error ) {
    alert('Erreur : ' + error.description);
  }
};
httpRequest.open('GET', 'loadPlayer', true);
httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
httpRequest.send();
});
var updateGhost;
var sendDamages;
var sendPostMortem;
var ressurectMe;
promise1.then(function(value) {
// ================================================
socket.on('message', function(message) {

    alert('Le serveur a un message pour vous : ' + message);

})
// ================================================
// MAIN
var game;
var myRoom = [];
var personalRoomId = false;
var myConfig = {};
var isPlayerAlreadySet = false;

socket.on('newPlayer',function(dataNewPlayer){
    var room = dataNewPlayer[0];
    var score = dataNewPlayer[1];
    var props = dataNewPlayer[2];
    if(!isPlayerAlreadySet){
        for(var i=0;i<room.length;i++){
            if(room[i].id == socket.id){
                myConfig = room[i];
                personalRoomId = room[i].id;
                game = new Game('canvas',myConfig,props);
                isPlayerAlreadySet = true;
                var rank = '<tr id="'+room[i]['id']+'">\
                  <td>'+room[i]['name']+'</td>\
                  <td>0</td>\
                  <td>0</td>\
                </tr>'
                $('#player_table').append(rank);
            }
        }
    }
    // Vérifie les joueurs qui se connectent
     checkIfNewGhost(room);

});
// Vérifie les joueurs qui se déconnectent
 socket.on('disconnectPlayer', function(room){
    checkIfGhostDisconnect(room);
 });
// ================================================


// ================================================
// EXTRA FUNCTIONS
var sortRoom = function(room){ // sort the players in room by id
    return room.sort(function(a, b) {
        var nameA = a.id.toUpperCase(); // ignore upper and lowercase
        var nameB = b.id.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    });
}
var checkIfNewGhost = function(room){ // check if there is a new ghost in room
    for(var i=0;i<room.length;i++){
        if(room[i].id != personalRoomId){
            var ghostAlreadyExist = false;
            for(var j=0;j<myRoom.length;j++){
                if(room[i].id == myRoom[j].id){
                    ghostAlreadyExist = true;
                    break;
                }
            }
            if(!ghostAlreadyExist){ // if ghost not exist yet in myRoom
                createGhost(room[i],room[i].id);
                console.log(room[i]);
            }
        }
    }
}
var checkIfGhostDisconnect = function(room){ // check if it miss a ghost in room
    for(var i=0;i<myRoom.length;i++){
        var ghostExist = false;
        for(var j=0;j<room.length;j++){
            if(myRoom[i].id == room[j].id){
                ghostExist = true;
                break;
            }
        }

        if(!ghostExist){ // if ghost not exist yet in myRoom
            deleteGhost(myRoom[i].id,i);
        }
    }
}
var createGhost = function(ghost,id){ // create a new ghost
  var rank = '\
  <tr id="'+ghost['id']+'">\
    <td>'+ghost['name']+'</td>\
    <td>0</td>\
    <td>0</td>\
  </tr>\
  '
  $('#player_table').append(rank);
    myRoom.push(ghost);
    newGhostPlayer = GhostPlayer(game,ghost,id);
    game._PlayerData.ghostPlayers.push(newGhostPlayer);
}
 updateGhost = function(data){ // update all the ghosts with room data
    socket.emit('updateData',[data,personalRoomId]);
}
var deleteGhost = function(index,position){ // delete the ghost by the index
    deleteGameGhost(game,index);
    myRoom.splice(position,1);
    // ICI fonction pour détruire le ghost du jeu
}
sendDamages = function(damage,target){ // update all the ghosts with room data
    socket.emit('distributeDamage',[damage, target, personalRoomId]);

}
sendPostMortem = function(whoKilledMe){ // update all the ghosts with room data
    if(!whoKilledMe){
        var whoKilledMe = personalRoomId;
    }
    socket.emit('killPlayer',[personalRoomId,whoKilledMe]);
}
ressurectMe = function(position){ // update all the ghosts with room data
    var dataToSend = [game._PlayerData.sendActualData(),personalRoomId];
    dataToSend[0].ghostCreationNeeded = true;
    dataToSend[0].myData = myData;
    socket.emit('updateData',dataToSend);
}

var destroyPropsToServer = function(idServer,type){ // update all the ghosts with room data
    socket.emit('updatePropsRemove',[idServer,type]);
}
// ================================================
socket.on('requestPosition', function(room){
    var dataToSend = [game._PlayerData.sendActualData(),personalRoomId];
    socket.emit('updateData',dataToSend);
});

socket.on ('updatePlayer', function (arrayData) {
    if(arrayData.id != personalRoomId){
        if(arrayData.ghostCreationNeeded){
            console.log(arrayData.ghostData);
            arrayData = arrayData.ghostData;
            var newGhostPlayer = GhostPlayer(game,arrayData,arrayData.id);
            game._PlayerData.ghostPlayers.push(newGhostPlayer);
        }else{
            game._PlayerData.updateLocalGhost(arrayData);
        }
    }
});

socket.on('requestPosition', function(room){
    var dataToSend = [game._PlayerData.sendActualData(),personalRoomId];
    socket.emit('updateData',dataToSend);
});

socket.on ('giveDamage', function (arrayData) {
  console.log('alors ?');
    if(arrayData[1] == personalRoomId){
        console.log('receive damage')
        game._PlayerData.getDamage(arrayData[0],arrayData[2]);
    }

});
socket.on ('killGhostPlayer', function (arrayData) {
    var idArray = arrayData[0];
    var roomScore = arrayData[1];
    if(idArray[0] != personalRoomId){
        deleteGameGhost(game,idArray[0]);
    }
    if(idArray[1] == personalRoomId){
        // game._PlayerData.newDeadEnnemy(idArray[2]);
    }
    // game.displayScore(roomScore);
});
socket.on ('ressurectGhostPlayer', function (idPlayer) {
    if(idPlayer != personalRoomId){
        deleteGameGhost(game,idPlayer);
    }
});
});
