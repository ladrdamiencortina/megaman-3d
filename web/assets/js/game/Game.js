Game = function(canvasId,myConfig) {
  // Canvas et engine défini ici
  var canvas = document.getElementById(canvasId);
  var engine = new BABYLON.Engine(canvas, true);
  this.engine = engine;
  var _this = this;
  _this.actualTime = Date.now();

    this.allSpawnPoints = [
        new BABYLON.Vector3(3*32, 0, 0),
        new BABYLON.Vector3(-3*32, 0, 0),
        new BABYLON.Vector3(0, 0, 3*32),
        new BABYLON.Vector3(0, 0, -3*32)
    ];

  // On initie la scène avec une fonction associé à l'objet Game
  this.scene = this._initScene(engine);

  var _arena = new Arena(_this);

  var _player = new Player(_this, canvas, myConfig);
  this._PlayerData = _player;

  // Permet au jeu de tourner
  engine.runRenderLoop(function () {
    _this.fps = Math.round(1000/engine.getDeltaTime());

    _player._checkMove((_this.fps)/60,_arena);

    _this.scene.render();
  });

  // Ajuste la vue 3D si la fenetre est agrandi ou diminué
  window.addEventListener("resize", function () {
    if (engine) {
      engine.resize();
    }
  },false);

  //TO DO déplacer dans un objet map
  // var audioElement = document.getElementById('music');
  // audioElement.setAttribute("preload", "auto");
  // audioElement.autobuffer = true;
  // audioElement.load();
  // audioElement.play();
window.addEventListener('keypress', (evt) => {
  if(evt.key == 'a'){
    console.log(1);
    if($('#messagePanel').hasClass('open')){
      $('#messagePanel').css("top","-50%");
      $('#messagePanel').removeClass('open');
    }else{
      $('#messagePanel').css("top","50%");
      $('#messagePanel').addClass('open');
    }
  }
})

};

Game.prototype = {
// Prototype d'initialisation de la scène
  _initScene : function(engine) {
    var scene = new BABYLON.Scene(engine);
    scene.clearColor=new BABYLON.Color3(0,0,0);
    scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
    scene.collisionsEnabled = true;
    return scene;
  },

}

// ------------------------- TRANSFO DE DEGRES/RADIANS
function degToRad(deg)
{
   return (Math.PI*deg)/180
}
// ----------------------------------------------------

// -------------------------- TRANSFO DE DEGRES/RADIANS
function radToDeg(rad)
{
   // return (Math.PI*deg)/180
   return (rad*180)/Math.PI
}
// ----------------------------------------------------
